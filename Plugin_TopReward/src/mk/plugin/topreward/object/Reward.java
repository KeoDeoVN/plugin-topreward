package mk.plugin.topreward.object;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class Reward {
	
	private long start;	
	private long longerval;
	private String placeholder;
	private List<String> commands = Lists.newArrayList();
	
	public Reward(long start, long longerval, String placeholder, List<String> commands) {
		this.placeholder = placeholder;
		this.longerval = longerval;
		this.commands = commands;
		this.start = start;
	}
	
	public long getStart() {
		return this.start;
	}
	
	public String getPlaceholder() {
		return this.placeholder;
	}
	
	public List<String> getCommands() {
		return this.commands;
	}
	
	public long getInterval() {
		return this.longerval;
	}
	
	// Static
	
	public static Map<String, List<Reward>> rewards = Maps.newHashMap();
	
}
