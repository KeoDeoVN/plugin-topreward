package mk.plugin.topreward.placeholder;

import org.bukkit.entity.Player;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import mk.plugin.topreward.main.MainTR;

public class TopRewardPlaceholder extends PlaceholderExpansion {

	@Override
	public String getAuthor() {
		return "MankaiStep";
	}

	@Override
	public String getIdentifier() {
		return "topreward";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}

	@Override
    public String onPlaceholderRequest(Player player, String s){
		if (s.startsWith("top_remain_")) {
			String top = s.replace("top_remain_", "");
			if (MainTR.timeCheckTasks.containsKey(top)) {
				return format(MainTR.timeCheckTasks.get(top).getRemain());
			} else return "Wrong Top ID";
		}
		
		
		return "Wrong placeholder";
	}
	
	private String format(long miliTime) {
		return (miliTime / (3600000 * 24)) + "d " + ((miliTime % (3600000 * 24)) / 3600000) + "h " + ((miliTime % 3600000) / 60000) + "m " + ((miliTime % 60000) / 1000) + "s";
	}
}
