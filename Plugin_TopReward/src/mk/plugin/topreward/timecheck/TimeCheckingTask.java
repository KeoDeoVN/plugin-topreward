package mk.plugin.topreward.timecheck;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class TimeCheckingTask extends BukkitRunnable {
	
	// 0 = 07:00 01/01/1970 Thursday 
	
	private long start;
	private long interval;
	private Runnable runnable;
	
	private long division;
	
	public TimeCheckingTask(Plugin plugin, long start, long interval, Runnable runnable) {
		this.start = start;
		this.interval = interval;
		this.runnable = runnable;
		this.division = this.getDivision();
		this.runTaskTimer(plugin, 0, 5);
	}
	
	public long getStart() {
		return this.start;
	}
	
	public long getInterval() {
		return this.interval;
	}
	
	public Runnable getRunnable() {
		return this.runnable;
	}

	public long getRemain() {
		return this.interval - ((System.currentTimeMillis() - this.start) % this.interval);
	}
	
	private long getDivision() {
		long current = System.currentTimeMillis();
		long sub = current - start;
		return sub / interval;
	}
	
	@Override
	public void run() {
		long d = getDivision();
		if (d > this.division) {
			this.division = d;
			// Start
			this.runnable.run();
		}
	}
	
	
	
	
}
