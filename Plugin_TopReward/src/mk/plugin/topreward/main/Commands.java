package mk.plugin.topreward.main;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import mk.plugin.topreward.object.Reward;

public class Commands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		if (!sender.hasPermission("tr.*")) return false;
		
		if (args[0].equalsIgnoreCase("reload")) {
			MainTR.getPlugin(MainTR.class).reloadConfig();
			sender.sendMessage("Reloaded");
			
//			Reward.rewards.forEach(reward -> {
//				sender.sendMessage(reward.getTime().toString() + " " + reward.getPlaceholder() + " " + reward.getPlaceholder() + reward.getCommands().toString());
//			});
			
		}
		
		else if (args[0].equalsIgnoreCase("reward")) {
			Reward.rewards.forEach((id, rr) -> {
				rr.forEach(reward -> {
					MainTR.give(reward);
				});
			});
			
		}
		
		return false;
	}

}
