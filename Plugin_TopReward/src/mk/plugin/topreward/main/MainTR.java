package mk.plugin.topreward.main;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import me.clip.placeholderapi.PlaceholderAPI;
import mk.plugin.topreward.object.Reward;
import mk.plugin.topreward.placeholder.TopRewardPlaceholder;
import mk.plugin.topreward.timecheck.TimeCheckingTask;

public class MainTR extends JavaPlugin {
	
	public static Map<String, TimeCheckingTask> timeCheckTasks = Maps.newHashMap();
	
	public FileConfiguration config;
	
	@Override
	public void onEnable() {
		// Init
		this.saveDefaultConfig();
		this.reloadConfig();
		this.getCommand("tr").setExecutor(new Commands());
		
		// Start tasks
		timeCheckTasks.clear();
		Reward.rewards.forEach((id, rr) -> {
			rr.forEach(r -> {
				timeCheckTasks.put(id, new TimeCheckingTask(this, r.getStart(), r.getInterval(), () -> {
					give(r);
				}));
			});
		});
		
		// Register placeholder
		new TopRewardPlaceholder().register();
	}
	
	public static void give(Reward r) {
		r.getCommands().forEach(cmd -> {
			String player = PlaceholderAPI.setPlaceholders(null, r.getPlaceholder());
			if (player == null || player.length() == 0) return;
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("%player%", player));
		});
	}
	
	public void reloadConfig() {
		config = YamlConfiguration.loadConfiguration(new File(this.getDataFolder(), "config.yml"));
		
		// Get
		Reward.rewards.clear();
		config.getConfigurationSection("topreward").getKeys(false).forEach(id -> {
			long start = config.getLong("topreward." + id + ".start");
			List<String> phs = config.getStringList("topreward." + id + ".placeholder");
			List<List<String>> cmds = Lists.newArrayList();
			
			config.getStringList("topreward." + id + ".reward").forEach(s -> {
				List<String> cs = Lists.newArrayList();
				for (String ss : s.split(";")) cs.add(ss);
				cmds.add(cs);
			});
			
			long interval = 86400000;
			if (!config.contains("topreward." + id + ".interval")) {
				config.set("topreward." + id + ".interval", interval);
			} interval = config.getLong("topreward." + id + ".interval");
			
			for (int i = 0 ; i < phs.size() ; i ++) {
				List<Reward> list = Reward.rewards.getOrDefault(id, null) == null ? Lists.newArrayList() : Reward.rewards.getOrDefault(id, null);
				list.add(new Reward(start, interval, phs.get(i), cmds.get(i)));
				Reward.rewards.put(id, list);
			}
		});
	}
	
}
